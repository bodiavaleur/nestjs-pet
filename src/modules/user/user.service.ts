import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { exclude } from '@helpers/prisma';
import { User } from '@prisma/client';

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) {}

  async me(userId: string) {
    const user = await this.prisma.user.findUnique({
      where: { id: userId },
    });

    const userWithoutSensitiveData = exclude<User>(user, [
      'password',
      'refreshToken',
    ]);

    return userWithoutSensitiveData;
  }
}
