import { Controller, Get } from '@nestjs/common';
import { UserService } from './user.service';
import { GetUser } from '../auth/decorators';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get('me')
  me(@GetUser() user) {
    return this.userService.me(user.sub);
  }
}
