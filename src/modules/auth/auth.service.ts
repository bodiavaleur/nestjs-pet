import {
  ForbiddenException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { AuthDto } from './dto';
import * as argon2 from 'argon2';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private config: ConfigService,
    private prisma: PrismaService,
    private jwt: JwtService,
  ) {}

  async signUp(dto: AuthDto) {
    try {
      const password = await argon2.hash(dto.password);

      const { id, email } = await this.prisma.user.create({
        data: { email: dto.email, password },
      });

      const tokens = await this.createTokens(id, email);
      await this.updateRefreshTokenHash(id, tokens.refreshToken);

      return tokens;
    } catch {
      throw new ForbiddenException('Unable to create an account');
    }
  }

  async signIn(dto: AuthDto) {
    const user = await this.prisma.user.findUnique({
      where: { email: dto.email },
    });

    if (!user) throw new NotFoundException();

    const isValidPassword = await argon2.verify(user.password, dto.password);

    if (!isValidPassword) throw new NotFoundException();

    const tokens = await this.createTokens(user.id, user.email);
    await this.updateRefreshTokenHash(user.id, tokens.refreshToken);

    return tokens;
  }

  async logout(userId: string) {
    await this.prisma.user.update({
      where: { id: userId, refreshToken: { not: null } },
      data: { refreshToken: null },
    });
  }

  async refreshTokens(userId: string, refreshToken: string) {
    const user = await this.prisma.user.findUnique({ where: { id: userId } });

    if (!user || !user.refreshToken) throw new UnauthorizedException();

    const isValidToken = await argon2.verify(user.refreshToken, refreshToken);

    if (!isValidToken) throw new UnauthorizedException();

    const tokens = await this.createTokens(user.id, user.email);
    await this.updateRefreshTokenHash(user.id, tokens.refreshToken);

    return tokens;
  }

  async createTokens(userId: string, email: string) {
    const payload = { sub: userId, email };

    const [accessToken, refreshToken] = await Promise.all([
      await this.jwt.signAsync(payload, {
        expiresIn: this.config.get('JWT_ACCESS_LIFETIME'),
        secret: this.config.get('JWT_ACCESS_SECRET'),
      }),
      await this.jwt.signAsync(payload, {
        expiresIn: this.config.get('JWT_REFRESH_LIFETIME'),
        secret: this.config.get('JWT_REFRESH_SECRET'),
      }),
    ]);

    return { accessToken, refreshToken };
  }

  async updateRefreshTokenHash(userId: string, refreshToken: string) {
    const hash = await argon2.hash(refreshToken);

    await this.prisma.user.update({
      where: { id: userId },
      data: { refreshToken: hash },
    });
  }
}
