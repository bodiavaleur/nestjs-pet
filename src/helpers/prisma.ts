/**
 * Exclude keys from model
 * @see https://www.prisma.io/docs/concepts/components/prisma-client/excluding-fields
 */
function exclude<T>(user: T, keys: (keyof T)[]) {
  return Object.fromEntries(
    Object.entries(user).filter(([key]) => !keys.includes(key as keyof T)),
  );
}

export { exclude };
